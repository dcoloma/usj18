INVALID_ARGUMENTS = 'The arguments were not valid';
NOT_A_TRIANGLE = 'Impossible to create a triangle with those sides';
EQUILATERAL = 'Equilateral triangle';
ISOSCELES = 'Isosceles triangle';
SCALENE = 'Scalene triangle';

/*
 * Given the three sides of a triangle it calculates the type of the
 * triangle.
 */
function getTriangleType(a, b, c) {
  let type = '';
  // In Javascript isNaN returns true if the argument is Not a Number (NaN)
  if ( isNaN(a) || isNaN(b) || isNaN(c) ) { //Inputs are not numbers
    type = INVALID_ARGUMENTS;
  } else { // Inputs are numbers
    // Convert from objects to float
    let a = parseFloat(a);
    let b = parseFloat(b);
    let c = parseFloat(c);
    if ((a <= 0) || (b <= 0) || (c <= 0)) { // A side is not positive
      type = INVALID_ARGUMENTS;
    } else if ( (a+b <= c) || (a+c <= b) || (b+c <= a)) { // invalid triangle
      type = NOT_A_TRIANGLE;
    } else { // All sides different = scalene
      if ( (a==b) && (b==c)) {
        type = EQUILATERAL;
      } else if ( (a==b) || (b==c) || (a==c)) {
        type = ISOSCELES;
      } else {
        type = SCALENE;
      }
    }
  }
  return type;
}
